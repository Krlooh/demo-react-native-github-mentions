import React from 'react'
import {
    Dimensions,
} from 'react-native';
import Colors from './Colors';
import Fonts from './Fonts';
import Borders from './Borders';
import Margins from './Margins';
export default {
    Colors,
    Fonts,
    Dimensions: Dimensions.get('window'),
    Margins,
    Borders
}