import DeviceInfo from './DeviceInfo';
import Regex from './Regex';
export default {
    DeviceInfo,
    Regex
};
