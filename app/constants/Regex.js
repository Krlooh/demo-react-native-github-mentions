export default {
    supportTags: /[@#]/,
    noSpaces: /[\n\r\s]/,
}