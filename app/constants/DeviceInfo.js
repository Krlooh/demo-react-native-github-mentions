import React from 'react';
import {
    Platform,
} from 'react-native';
const OS_VERSION = Platform.Version;
const IOS = Platform.OS == 'ios';
export default {
    IOS,
    OS_VERSION
}