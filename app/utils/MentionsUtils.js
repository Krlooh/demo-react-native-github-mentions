import isNumber from 'lodash/isNumber'
import keys from 'lodash/keys'

const PLACEHOLDERS = {
    id: '__id__',
    display: '__display__',
    type: '__type__',
}
const markup = '@[__display__](__type__:__id__)';
const displayTransform = (id, display, type) => (display);
const numericComparator = function (a, b) {
    a = a === null ? Number.MAX_VALUE : a
    b = b === null ? Number.MAX_VALUE : b
    return a - b
}


export const escapeRegex = str => str.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&')
const markupToRegex = () => {
    let markupPattern = markup.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&')
    markupPattern = markupPattern.replace(PLACEHOLDERS.display, '(.+?)')
    markupPattern = markupPattern.replace(PLACEHOLDERS.id, '(.+?)')
    markupPattern = markupPattern.replace(PLACEHOLDERS.type, '(.+?)')
    return new RegExp(markupPattern, 'g')
}
const regexMarkup = markupToRegex();

export const spliceString = (str, start, end, insert) =>
    str.substring(0, start) + insert + str.substring(end)

/**
 * parameterName: "id", "display", or "type"
 * TODO: This is currently only exported for testing
 */
export const getPositionOfCapturingGroup = (parameterName) => {
    if (
        parameterName !== 'id' &&
        parameterName !== 'display' &&
        parameterName !== 'type'
    ) {
        throw new Error("parameterName must be 'id', 'display', or 'type'")
    }

    // calculate positions of placeholders in the markup
    let indexDisplay = markup.indexOf(PLACEHOLDERS.display)
    let indexId = markup.indexOf(PLACEHOLDERS.id)
    let indexType = markup.indexOf(PLACEHOLDERS.type)

    // set indices to null if not found
    if (indexDisplay < 0) indexDisplay = null
    if (indexId < 0) indexId = null
    if (indexType < 0) indexType = null

    if (indexDisplay === null && indexId === null) {
        // markup contains none of the mandatory placeholders
        throw new Error(
            'The markup `' +
            markup +
            '` must contain at least one of the placeholders `__id__` or `__display__`'
        )
    }

    if (indexType === null && parameterName === 'type') {
        // markup does not contain optional __type__ placeholder
        return null
    }

    // sort indices in ascending order (null values will always be at the end)
    const sortedIndices = [indexDisplay, indexId, indexType].sort(
        numericComparator
    )

    // If only one the placeholders __id__ and __display__ is present,
    // use the captured string for both parameters, id and display
    if (indexDisplay === null) indexDisplay = indexId
    if (indexId === null) indexId = indexDisplay

    if (parameterName === 'id') return sortedIndices.indexOf(indexId)
    if (parameterName === 'display') return sortedIndices.indexOf(indexDisplay)
    if (parameterName === 'type')
        return indexType === null ? null : sortedIndices.indexOf(indexType)
}

const idPos = getPositionOfCapturingGroup('id');
const displayPos = getPositionOfCapturingGroup('display');
const typePos = getPositionOfCapturingGroup('type');

// Finds all occurences of the markup in the value and iterates the plain text sub strings
// in between those markups using `textIteratee` and the markup occurrences using the
// `markupIteratee`.
export const iterateMentionsMarkup = (
    value,
    textIteratee,
    markupIteratee,
) => {

    let match;
    let start = 0;
    let currentPlainTextIndex = 0;

    // detect all mention markup occurences in the value and iterate the matches
    while ((match = regexMarkup.exec(value)) !== null) {
        let id = match[idPos + 1]
        let display = match[displayPos + 1]
        let type = typePos !== null ? match[typePos + 1] : null

        if (displayTransform) display = displayTransform(id, display, type)

        let substr = value.substring(start, match.index)
        textIteratee(substr, start, currentPlainTextIndex)
        currentPlainTextIndex += substr.length

        markupIteratee(
            match[0],
            match.index,
            currentPlainTextIndex,
            id,
            display,
            type,
            start
        )
        currentPlainTextIndex += display.length

        start = regexMarkup.lastIndex
    }

    if (start < value.length) {
        textIteratee(value.substring(start), start, currentPlainTextIndex)
    }
}

// For the passed character index in the plain text string, returns the corresponding index
// in the marked up value string.
// If the passed character index lies inside a mention, the value of `inMarkupCorrection` defines the
// correction to apply:
//   - 'START' to return the index of the mention markup's first char (default)
//   - 'END' to return the index after its last char
//   - 'NULL' to return null
export const mapPlainTextIndex = (
    value,
    indexInPlainText,
    inMarkupCorrection = 'START',
) => {
    if (!isNumber(indexInPlainText)) {
        return indexInPlainText
    }

    let result
    let textIteratee = function (substr, index, substrPlainTextIndex) {
        if (result !== undefined) return

        if (substrPlainTextIndex + substr.length >= indexInPlainText) {
            // found the corresponding position in the current plain text range
            result = index + indexInPlainText - substrPlainTextIndex
        }
    }
    let markupIteratee = function (
        markup,
        index,
        mentionPlainTextIndex,
        id,
        display,
        type,
        lastMentionEndIndex
    ) {
        if (result !== undefined) return

        if (mentionPlainTextIndex + display.length > indexInPlainText) {
            // found the corresponding position inside current match,
            // return the index of the first or after the last char of the matching markup
            // depending on whether the `inMarkupCorrection`
            if (inMarkupCorrection === 'NULL') {
                result = null
            } else {
                result = index + (inMarkupCorrection === 'END' ? markup.length : 0)
            }
        }
    }

    iterateMentionsMarkup(
        value,
        textIteratee,
        markupIteratee,
    )

    // when a mention is at the end of the value and we want to get the caret position
    // at the end of the string, result is undefined
    return result === undefined ? value.length : result
}


// Applies a change from the plain text textarea to the underlying marked up value
// guided by the textarea text selection ranges before and after the change
export const applyChangeToValue = (
    value,
    plainTextValue,
    selectionStartBeforeChange,
    selectionEndBeforeChange,
    selectionEndAfterChange,
) => {
    let oldPlainTextValue = getPlainText(value)

    let lengthDelta = oldPlainTextValue.length - plainTextValue.length

    // Fixes an issue with replacing combined characters for complex input. Eg like acented letters on OSX
    if (
        selectionStartBeforeChange === selectionEndBeforeChange &&
        selectionEndBeforeChange === selectionEndAfterChange &&
        oldPlainTextValue.length === plainTextValue.length
    ) {
        selectionStartBeforeChange = selectionStartBeforeChange - 1
    }

    // extract the insertion from the new plain text value
    let insert = plainTextValue.slice(
        selectionStartBeforeChange,
        selectionEndAfterChange
    )

    // handling for Backspace key with no range selection
    let spliceStart = Math.min(
        selectionStartBeforeChange,
        selectionEndAfterChange
    )

    let spliceEnd = selectionEndBeforeChange
    if (selectionStartBeforeChange === selectionEndAfterChange) {
        // handling for Delete key with no range selection
        spliceEnd = Math.max(
            selectionEndBeforeChange,
            selectionStartBeforeChange + lengthDelta
        )
    }

    let mappedSpliceStart = mapPlainTextIndex(
        value,
        spliceStart,
        'START',
    )
    let mappedSpliceEnd = mapPlainTextIndex(
        value,
        spliceEnd,
        'END',
    )

    let controlSpliceStart = mapPlainTextIndex(
        value,
        spliceStart,
        'NULL',
    )
    let controlSpliceEnd = mapPlainTextIndex(
        value,
        spliceEnd,
        'NULL',
    )
    let willRemoveMention =
        controlSpliceStart === null || controlSpliceEnd === null

    let newValue = spliceString(value, mappedSpliceStart, mappedSpliceEnd, insert)

    if (!willRemoveMention) {
        // test for auto-completion changes
        let controlPlainTextValue = getPlainText(newValue)
        if (controlPlainTextValue !== plainTextValue) {
            // some auto-correction is going on

            // find start of diff
            spliceStart = 0
            while (plainTextValue[spliceStart] === controlPlainTextValue[spliceStart])
                spliceStart++

            // extract auto-corrected insertion
            insert = plainTextValue.slice(spliceStart, selectionEndAfterChange)

            // find index of the unchanged remainder
            spliceEnd = oldPlainTextValue.lastIndexOf(
                plainTextValue.substring(selectionEndAfterChange)
            )

            // re-map the corrected indices
            mappedSpliceStart = mapPlainTextIndex(
                value,
                spliceStart,
                'START',
            )
            mappedSpliceEnd = mapPlainTextIndex(
                value,
                spliceEnd,
                'END',
            )
            newValue = spliceString(value, mappedSpliceStart, mappedSpliceEnd, insert)
        }
    }

    return newValue
}

export const getPlainText = (value) => {
    return value.replace(regexMarkup, function () {
        // first argument is the whole match, capturing groups are following
        let id = arguments[idPos + 1]
        let display = arguments[displayPos + 1]
        let type = arguments[typePos + 1]
        if (displayTransform) display = displayTransform(id, display, type)
        return display
    })
}

export const getMentions = (value) => {
    let mentions = []
    iterateMentionsMarkup(
        value,
        function () { },
        function (match, index, plainTextIndex, id, display, type, start) {
            mentions.push({
                id: id,
                display: display,
                type: type,
                index: index,
                plainTextIndex: plainTextIndex,
            })
        },
    )
    return mentions
}


export const makeMentionsMarkup = (id, display, type) => {
    let result = markup.replace(PLACEHOLDERS.id, id)
    result = result.replace(PLACEHOLDERS.display, display)
    result = result.replace(PLACEHOLDERS.type, type)
    return result
}
