import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
} from 'react-native';
import Style from '../../styles';
import Strings from '../../strings/InputPost.json';
import InputTags from './InputTags';
import Button from '../Button';
export default class InputPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPostable: false
        }
    }
    _onPost = () => {
        const value = this.inputAdd.getValueToPost();
        console.warn(value);
    }
    _toggleIsPosteable = (isPostable) => {
        this.setState({ isPostable })
    }
    render() {
        return (
            <View style={styles.container}>
                <InputTags
                    ref={ref => { this.inputAdd = ref }}
                    toggleIsPosteable={this._toggleIsPosteable} />
                <Button
                    disabled={!this.state.isPostable}
                    style={styles.buttonPost}
                    onPress={this._onPost}>
                    <Text style={styles.textPost}>{Strings.post}</Text>
                </Button>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    buttonPost: {
        backgroundColor: Style.Colors.blue,
        position: 'absolute',
        alignSelf: 'center',
        right: Style.Margins.big,
        padding: Style.Margins.big,
        borderRadius: Style.Margins.mid
    },
    textPost: {
        color: Style.Colors.white,
        fontWeight: 'bold'
    }
});