import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {
    View,
    ActivityIndicator,
    StyleSheet,
} from 'react-native';
import { QueryRenderer, graphql } from 'react-relay';
import Style from '../../../styles';
import RelayStore from '../../../stores/RelayStore';
import UserMatches from './UserMatches';
import Text from '../../Text';
const ErrorView = ({ error }) => (
    <View>
        <Text>{error}</Text>
    </View>
)
const queries = {
    "mention": {
        query: graphql`
        query QueryRendererMatchesUsersQuery($query:String!, $type:SearchType!, $first:Int!, $size: Int!) {
          search(query:$query, type:$type, first:$first){
            edges{
              node{
                ... on User {
                  id
                  name
                  avatarUrl(size:$size)
                }
              }
            }
          }
        }
        `,
        variables: {
            "type": "USER",
            "first": 5,
            "query": "type:user in:name",
            "size": 80
        },
        listElement: UserMatches
    },
    //"hashtag": {}
}
export default QueryRendererMatches = (props) => {
    _onSelectTag = (data) => props.onSelectTag(data);
    _renderCoincidences = (resGraphQL) => {
        const error = resGraphQL.error;
        const resProps = resGraphQL.props;
        if (error) {
            return <ErrorView error={error} />;
        } else if (resProps) {
            const ListElement = queries[props.typeTag].listElement;
            return <ListElement onSelectTag={_onSelectTag} matches={resProps.search.edges} search={props.value} />;
        }
        return <ActivityIndicator />;
    }
    return (
        <View style={styles.container}>
            <View style={styles.viewInContainer}>
                {(props.value.length > 0 && props.typeTag && queries[props.typeTag]) &&
                    <QueryRenderer
                        environment={RelayStore}
                        variables={{
                            ...queries[props.typeTag].variables,
                            "query": `${queries[props.typeTag].variables.query} ${props.value}`
                        }}
                        query={queries[props.typeTag].query}
                        render={_renderCoincidences}
                    />
                }
            </View>
        </View>
    )
}
QueryRendererMatches.propTypes = {
    typeTag: PropTypes.string,
    value: PropTypes.string
};
const styles = StyleSheet.create({
    viewInContainer: {
        position: "absolute",
        alignSelf: "flex-end",
        width: Style.Dimensions.width - Style.Margins.min * 2,
    },
});