import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types';
import {
    View,
    TextInput,
    StyleSheet,
} from 'react-native';
import { getMentions, getPlainText, applyChangeToValue, makeMentionsMarkup } from '../../../utils/MentionsUtils';
import Constants from '../../../constants';
import Style from '../../../styles';
import Text from '../../Text';
import QueryRendererMatches from './QueryRendererMatches';
const charTag = {
    '@': 'mention',
    //   '#': 'hashtag'
}
export default class InputTags extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valueTag: '',
            typeTag: '',
            value: '',
        };
        this.value = '';
        this.auxValue = '';
        this.selection = { start: 0, end: 0 };
        this.prevSelection = { start: 0, end: 0 };
        this.positionTag = null;
        this.dataTags = [];
        this.valueTag = '';
        this.typeTag = '';
    }
    _onSelectionChange = ({ nativeEvent }) => {
        this.prevSelection = this.selection;
        this.selection = nativeEvent.selection;
        !Constants.DeviceInfo.IOS && this._valueInputChanged();
    }

    _valueInputChanged() {
        if (this.value != this.auxValue && this.value) {
            const newValue = applyChangeToValue(this.state.value, this.value, this.prevSelection.start, this.prevSelection.end, this.selection.end);
            this.setState({
                value: newValue,
            }, () => this._searchingForTag());
        } else {
            this._searchingForTag();
        }
        this.auxValue = this.value;
    }

    // busca un tag a su izquierda, y si esta soportado y no tiene espacios en blanco entre medias se habilita    
    _searchingForTag() {
        if (
            this.selection.end == this.selection.start
            && this.selection.end
            && this.value
        ) {
            let arrayTags = getMentions(this.state.value);
            if (arrayTags.length) {
                let indexValueInput = arrayTags.findIndex(item => item.plainTextIndex < this.selection.end && (item.plainTextIndex + item.display.length) > this.selection.end);
                if (indexValueInput > -1) {
                    this._onPressMention(arrayTags[indexValueInput]);
                }
            }
            const wordTillEnd = this.value.substring(0, this.selection.end);
            if (Constants.Regex.supportTags.test(wordTillEnd)) {
                const reverseWord = wordTillEnd.split("").reverse().join("");
                const tag = reverseWord.match(Constants.Regex.supportTags)[0];
                if (tag) {
                    const indexTag = wordTillEnd.lastIndexOf(tag);
                    let wordTillTag = this.value.substring(indexTag, wordTillEnd.length);
                    wordTillTag = wordTillTag.substring(1, wordTillTag.length);
                    if (wordTillTag.length > 1 && !Constants.Regex.noSpaces.test(wordTillTag)) {
                        if (this.searchTimeOut) {
                            clearTimeout(this.searchTimeOut);
                            delete this.searchTimeOut;
                        }
                        this.positionTag = indexTag;
                        this.valueTag = wordTillTag;
                        this.typeTag = charTag[tag];
                        this.searchTimeOut = setTimeout(() => {
                            this.setState({
                                typeTag: charTag[tag],
                                valueTag: wordTillTag
                            });
                            delete this.searchTimeOut;
                        }, 500);
                    } else {
                        this._noTagActive();
                    }
                } else {
                    this._noTagActive();
                }
            } else {
                this._noTagActive();
            }
        } else {
            this._noTagActive();
        }
    }
    _noTagActive() {
        if (this.searchTimeOut) {
            clearTimeout(this.searchTimeOut);
            delete this.searchTimeOut;
        }
        if (this.typeTag) {
            this.setState({
                valueTag: '',
                typeTag: ''
            });
            this.valueTag = '';
            this.typeTag = '';
            this.positionTag = null;
        }
    }
    _onChangeText = (value) => {
        this.value = value;
        if (value) {
            if (Constants.DeviceInfo.IOS) {
                this._valueInputChanged();
            } else if (value.length === this.auxValue.length) {
                this._valueInputChanged();
            }
            if (this.auxValue.length == 0)
                this.props.toggleIsPosteable(true);
        } else {
            this.setState({
                valueTag: '',
                typeTag: '',
                arrayTags: [],
                value: '',
            });
            this.valueTag = '';
            this.typeTag = null;
            this.props.toggleIsPosteable(false);
        }
    }
    _onSelectTag = (data) => {
        const { value, typeTag } = this.state;
        const makeTagMarkup = makeMentionsMarkup(data.id, data.name, typeTag);
        const newPlainText = this.value.substring(0, this.positionTag) + data.name + ' ' + this.value.substring(this.positionTag + this.valueTag.length + 1, this.value.length);
        const arrayTags = getMentions(value);
        let plusIndex = 0;
        let flag = false;
        let indexMentions = arrayTags.length;
        while (!flag && indexMentions--) {
            if (this.positionTag > arrayTags[indexMentions].plainTextIndex) {
                // añade 6 caracteres que son @[](): + type + id
                plusIndex = plusIndex + 6 + arrayTags[indexMentions].type.length + arrayTags[indexMentions].id.length;
            } else {
                flag = true;
            }
        }
        let newPositionTag = this.positionTag + plusIndex;
        const newValue = value.substring(0, newPositionTag) + makeTagMarkup + ' ' + value.substring(newPositionTag + this.valueTag.length + 1, value.length);
        this.value = newPlainText;
        this.auxValue = newPlainText;
        this.positionTag = null;
        this.valueTag = '';
        this.typeTag = '';
        this.setState({
            value: newValue,
            valueTag: '',
            typeTag: '',
        });
    }
    _onPressMention(item) {
        console.warn(item);
    }
    _renderInputValue() {
        const { value } = this.state;
        const plainText = getPlainText(value);
        const arrayTags = getMentions(value);
        if (arrayTags.length) {
            let endCurrentValue = 0
            return arrayTags.map((item, i) => {
                const end = item.plainTextIndex + item.display.length;
                if (item.plainTextIndex == endCurrentValue) {
                    if (i == arrayTags.length - 1 && end != plainText.length) {
                        return (
                            <Fragment key={i}>
                                <Text style={styles.tag}>{item.display}</Text>
                                <Text>{plainText.substring(end, plainText.length)}</Text>
                            </Fragment>
                        )
                    } else {
                        endCurrentValue = end;
                        return (
                            <Text
                                key={i}
                                style={styles.tag}>{item.display}</Text>
                        )
                    }
                } else {
                    const elementValue = plainText.substring(endCurrentValue, item.plainTextIndex);
                    if (i == arrayTags.length - 1 && end != plainText.length) {
                        return (
                            <Fragment key={i}>
                                <Text>{elementValue}</Text>
                                <Text style={styles.tag}>{item.display}</Text>
                                <Text>{plainText.substring(end, plainText.length)}</Text>
                            </Fragment>
                        )
                    } else {
                        endCurrentValue = end;
                        return (
                            <Fragment key={i}>
                                <Text>{elementValue}</Text>
                                <Text style={styles.tag}>{item.display}</Text>
                            </Fragment>
                        )
                    }
                }
            });
        } else {
            return (<Text>{plainText}</Text>)
        }
    }
    getValueToPost() {
        return this.state.value;
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerInputTag}>
                    <TextInput
                        //value={this.state.value}
                        multiline={true}
                        underlineColorAndroid={'transparent'}
                        style={styles.inputTags}
                        onSelectionChange={this._onSelectionChange}
                        onChangeText={this._onChangeText} >
                        {this._renderInputValue()}
                    </TextInput>
                </View>
                <View>
                    <QueryRendererMatches
                        onSelectTag={this._onSelectTag}
                        value={this.state.valueTag}
                        typeTag={this.state.typeTag}
                    />
                </View>
            </View>
        )
    }
}
InputTags.propTypes = {
    toggleIsPosteable: PropTypes.func,
}
const styles = StyleSheet.create({
    container: {
        margin: Style.Margins.min,
        width: Style.Dimensions.width - Style.Margins.min * 2
    },
    containerInputTag: {
        borderWidth: Style.Borders.low,
        borderColor: Style.Colors.grayWhite,
        borderRadius: Style.Margins.big,
    },
    inputTags: {
        //color: 'transparent',
        width: Style.Dimensions.width - Style.Margins.min * 2 - Style.Fonts.mid * 4,
        minHeight: Style.Fonts.mid * 4
    },
    tag: {
        color: Style.Colors.blue,
        backgroundColor: Style.Colors.blueWhite
    },
});