import React, { Component } from 'react'
import {
    Image,
    View,
    StyleSheet,
    ScrollView
} from 'react-native';
import Button from '../../Button';
import Text from '../../Text';
import Style from '../../../styles';
import Utils from '../../../utils';
export default UserElement = (props) => {
    const search = Utils.RemoveDiacritics(props.search.toLowerCase());
    const searchLen = search.length;
    const highlightsText = (nameMatched) => {
        const nameMatchedLen = nameMatched.length;
        const nameMatchedFormated = Utils.RemoveDiacritics(nameMatched.toLowerCase());
        let reactTexts = [],
            lastChar = 0,
            startIndex = 0,
            index,
            matchToHighlights = [];
        while ((index = nameMatchedFormated.indexOf(search, startIndex)) > -1) {
            matchToHighlights.push(index);
            startIndex = index + searchLen;
        }
        const matchToHighlightsLen = matchToHighlights.length;
        matchToHighlights.map((item, i) => {
            const begin = item;
            const end = begin + searchLen;
            if (lastChar < begin) {
                reactTexts.push(
                    <Text key={'nh' + i}>{nameMatched.substring(lastChar, begin)}</Text>,
                    <Text
                        key={'h' + i}
                        style={styles.highlights}>{nameMatched.substring(begin, end)}</Text>
                )
            } else {
                reactTexts.push(
                    <Text
                        key={'h' + i}
                        style={styles.highlights}>{nameMatched.substring(begin, end)}</Text>,
                )
            }
            lastChar = end;
            if (matchToHighlightsLen - 1 == i && lastChar < nameMatchedLen) {
                reactTexts.push(
                    <Text key={'nh' + i}>{nameMatched.substring(lastChar, nameMatchedLen)}</Text>
                )
            }
        });
        return reactTexts;
    }
    return (
        <ScrollView
            keyboardShouldPersistTaps={'always'}
            style={styles.scrollView}>
            {props.matches.map((item, i) =>
                <Button
                    key={i}
                    style={styles.container}
                    onPress={() => props.onSelectTag(item.node)}>
                    <View>
                        <Image
                            style={styles.avatar}
                            resizeMode={Image.resizeMode.cover}
                            source={{ uri: item.node.avatarUrl }} />
                    </View>
                    <View>
                        <Text>{highlightsText(item.node.name)}</Text>
                    </View>
                </Button >
            )}
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    scrollView: {
        maxHeight: (50 + Style.Margins.mid) * 4,
        borderWidth: Style.Borders.low,
        borderColor: Style.Colors.grayWhite
    },
    container: {
        flexDirection: "row",
        alignItems: "center"
    },
    avatar: {
        height: 50,
        width: 50,
        marginRight: Style.Margins.mid,
        marginTop: Style.Margins.low
    },
    highlights: {
        fontWeight: "bold"
    },
    highlightsLow: {
        fontWeight: "bold",
        fontSize: Style.Fonts.low,
    },
    lowMatch: {
        fontSize: Style.Fonts.low,
        color: Style.Colors.gray
    }
});