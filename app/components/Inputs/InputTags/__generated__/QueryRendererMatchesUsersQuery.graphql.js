/**
 * @flow
 * @relayHash e0cad9524307a3529a5f95ef2365dd88
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type SearchType = "ISSUE" | "REPOSITORY" | "USER" | "%future added value";
export type QueryRendererMatchesUsersQueryVariables = {|
  query: string,
  type: SearchType,
  first: number,
  size: number,
|};
export type QueryRendererMatchesUsersQueryResponse = {|
  +search: {|
    +edges: ?$ReadOnlyArray<?{|
      +node: ?{|
        +id?: string,
        +name?: ?string,
        +avatarUrl?: any,
      |}
    |}>
  |}
|};
*/


/*
query QueryRendererMatchesUsersQuery(
  $query: String!
  $type: SearchType!
  $first: Int!
  $size: Int!
) {
  search(query: $query, type: $type, first: $first) {
    edges {
      node {
        __typename
        ... on User {
          id
          name
          avatarUrl(size: $size)
        }
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "query",
    "type": "String!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "type",
    "type": "SearchType!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "size",
    "type": "Int!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "query",
    "variableName": "query",
    "type": "String!"
  },
  {
    "kind": "Variable",
    "name": "type",
    "variableName": "type",
    "type": "SearchType!"
  }
],
v2 = {
  "kind": "InlineFragment",
  "type": "User",
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "name",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "avatarUrl",
      "args": [
        {
          "kind": "Variable",
          "name": "size",
          "variableName": "size",
          "type": "Int"
        }
      ],
      "storageKey": null
    }
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "QueryRendererMatchesUsersQuery",
  "id": null,
  "text": "query QueryRendererMatchesUsersQuery(\n  $query: String!\n  $type: SearchType!\n  $first: Int!\n  $size: Int!\n) {\n  search(query: $query, type: $type, first: $first) {\n    edges {\n      node {\n        __typename\n        ... on User {\n          id\n          name\n          avatarUrl(size: $size)\n        }\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "QueryRendererMatchesUsersQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "search",
        "storageKey": null,
        "args": v1,
        "concreteType": "SearchResultItemConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "SearchResultItemEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": null,
                "plural": false,
                "selections": [
                  v2
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "QueryRendererMatchesUsersQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "search",
        "storageKey": null,
        "args": v1,
        "concreteType": "SearchResultItemConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "SearchResultItemEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": null,
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "__typename",
                    "args": null,
                    "storageKey": null
                  },
                  v2
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'd7d92eeac2d02559c6113717875a1c0c';
module.exports = node;
