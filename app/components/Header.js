import React from 'react'
import {
    Text,
    StyleSheet,
    Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Style from '../styles';
export default Header = ({ text }) => (
    <Text style={styles.header}>
        {text}
    </Text>
)
Header.propTypes = {
    text: PropTypes.string
};
const styles = StyleSheet.create({
    header: {
        paddingTop: Style.Margins.big + Style.Margins.statusHeigth,
        paddingBottom: Style.Margins.big,
        textAlign: "center",
        ...Platform.select({
            ios: {
                shadowOffset: { width: 0, height: Style.Borders.mid },
                shadowColor: '#000',
                shadowOpacity: 0.25,
                shadowRadius: 6,
                zIndex: 1,
            },
            android: {
                elevation: 3
            },
        }),
        backgroundColor: Style.Colors.blueWhite,
        fontSize: Style.Fonts.big,
        fontWeight: 'bold',
        marginBottom: Style.Margins.low
    },
});