import React from 'react';
import {
    Text,
    StyleSheet
} from 'react-native';
import Style from '../styles';
export default CustomText = (props) => (
    <Text
        style={[styles.text, props.style]}
        selectable={props.selectable}>
        {props.children}
    </Text>
);
CustomText.defaultProps = {
    selectable: false,
};
const styles = StyleSheet.create({
    text: {
        color: Style.Colors.black,
        fontSize: Style.Fonts.mid,
    },
});