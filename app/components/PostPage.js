import React from 'react'
import {
    View,
} from 'react-native';
import Strings from '../strings/PostPage.json';
import InputPost from './Inputs/InputPost';
import Header from './Header';
export default PostPage = (props) => {
    return (
        <View>
            <Header text={Strings.header} />
            <InputPost />
        </View>
    )
}