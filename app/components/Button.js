import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    TouchableOpacity,
    TouchableNativeFeedback,
    StyleSheet,
    ViewPropTypes
} from 'react-native';
import { IOS, OS_VERSION } from '../constants/DeviceInfo';
export default Button = (props) => {
    if (props.disabled) {
        return (
            <View style={[props.style, styles.buttonDisabled]}>
                {props.children}
            </View>
        );
    } else if (!IOS && OS_VERSION > 19) {
        return (
            <TouchableNativeFeedback {...props}>
                {props.children}
            </TouchableNativeFeedback>
        )
    } else {
        return (
            <TouchableOpacity {...props}>
                {props.children}
            </TouchableOpacity>
        )
    }
};
Button.propTypes = {
    activeOpacity: PropTypes.number,
    style: ViewPropTypes.style,
    onPress: PropTypes.func,
    disabled: PropTypes.bool
};
Button.defaultProps = {
    activeOpacity: 0.9,
}
const styles = StyleSheet.create({
    buttonDisabled: {
        opacity: 0.7
    },
});