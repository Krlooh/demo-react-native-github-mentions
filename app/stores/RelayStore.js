import { Environment, Network, RecordSource, Store } from "relay-runtime";
function fetchQuery(operation, variables) {
    return fetch("https://api.github.com/graphql", {
        method: "POST",
        headers: {
            'Authorization': 'Bearer fbf46ba532433009909b5ea8350c7ec2082b59e0', 
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            query: operation.text,
            variables
        })
    }).then(response => {
        return response.json();
    }).catch((err) => {
        console.warn("fetchQuery error:", err);
        return err; 
     })
}
const env = new Environment({
    network: Network.create(fetchQuery),
    store: new Store(new RecordSource()),
});
export default env;
