import React from 'react';
import {
    Animated,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { IOS } from './constants/DeviceInfo';
export default StackNavigator(
    {

    },
    {
        headerMode: "none",
        navigationOptions: {
            gesturesEnabled: false,
        },
        initialRouteName: 'SplashPage',
        transitionConfig: IOS ? undefined : () => ({
            transitionSpec: {
                duration: 250,
                timing: Animated.timing,
            },
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps
                const { index } = scene
                const width = layout.initWidth;
                const translateX = position.interpolate({
                    inputRange: [index - 1, index, index + 1],
                    outputRange: [width, 0, 0],
                });
                return { transform: [{ translateX }] }
            },
        }),
    }
);