import React from 'react'
import {
  View,
  StyleSheet
} from 'react-native';
import Style from './styles';
import PostPage from './components/PostPage'
export default App = (props) => (
  <View style={styles.container}>
    <PostPage />
  </View>
)
const styles = StyleSheet.create({
  container: {
    backgroundColor: Style.Colors.white,
    height: Style.Dimensions.height,
    width: Style.Dimensions.width
  },
});